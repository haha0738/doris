<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Word;
class IndexController extends Controller
{
    public function index(){
    	$words = Word::all();
    	$arr = [];
    	foreach ($words as $word) {
    		$arr[] = $word->name . ' : ' . $word->content;
    	}
    	return view('index', ['words' => $arr]);
    }
}

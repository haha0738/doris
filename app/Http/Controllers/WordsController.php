<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Word;
class WordsController extends Controller
{
	public function index(){
		$words = Word::paginate(25);
		return view('words.index', ['words' => $words]);
	}
    public function create(){
    	return view('words.create');
    }

    public function store(Request $request){
    	$v = $this->validate($request, [
	        'name' => 'required',
	        'content' => 'required',
	    ]);
    	
    	if ($v && $v->fails())
	    {
	        return redirect()->back()->withErrors($v->errors());
	    }

	    Word::create($request->all());
	    return redirect()->action('IndexController@index');
    }

}

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<script type="text/javascript" src="{{asset('js/bundle.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">

	<link href='https://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
	<title>緩懷朵粒濕</title>
</head>
<body>
	<div class="container">

	@if($errors->count() > 0)
		<div class="alert alert-danger" role="alert">
		@foreach($errors->all() as $error)
			<li>{{$error}}</li>
		@endforeach
		</div>
	@endif
		@yield('content')
	</div>
</body>
</html>
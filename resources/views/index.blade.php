
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta property="og:title" content="緬懷朵粒濕"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="http://210.71.170.45:8000/"/>
	<meta property="og:image" content="https://graph.facebook.com/100000296568317/picture?type=square&width=200&height=200"/>
	<meta property="og:site_name" content="緬懷朵粒濕"/>
	<meta property="og:description" content="朵粒濕就要離開了，讓我們緬懷他噠噠的鍵盤聲了！"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<script type="text/javascript" src="{{asset('js/bundle.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">

	<title>緩懷朵粒濕</title>
	<style type="text/css">
	
	</style>
</head>
<body class="bg">

<div id="backgroundaudio">
<i class="fa fa-volume-up"></i>
<audio autoplay="" controls="" loop="" preload="">
 <source src="{{asset('audios/See You Again - Wiz Khalifa & Charlie Puth (Acoustic Cover) by Tiffany Alvord.mp3')}}" type="audio/mpeg"></source>
 Your browser does not support the audio element.
</audio>
</div>
<div class="container">
	<div class="main">
		<img src="https://graph.facebook.com/100000296568317/picture?type=square&width=200&height=200" class="img-circle">
		<h1 class="large-fnt">This is Doris</h1>
		<p class="leaving">She left us....</p>
		<h2>2015年11月26日 - 2016年03月31日</h2>
		<h2>他已經離開我們<span class="countdown"></span>了</h2>
		<h2>Words to Doris</h2>
		<h3><span class="typed"></span></h3>
		<a class="leave-message" href="{{action('WordsController@create')}}">留言給朵粒濕</a>		
	</div>

	
</div>
<script type="text/javascript">
	function getTimeRemaining(endtime){
	  var t = new Date() - Date.parse(endtime);
	  var seconds = Math.floor( (t/1000) % 60 );
	  var minutes = Math.floor( (t/1000/60) % 60 );
	  var hours = Math.floor( (t/(1000*60*60)) % 24 );
	  var days = Math.floor( t/(1000*60*60*24) );
	  return {
	    'total': t,
	    'days': days,
	    'hours': hours,
	    'minutes': minutes,
	    'seconds': seconds
	  };
	}

	function countdown() {
		var remain = getTimeRemaining('2016-03-31T18:00:00');
		
		$('.countdown').html(remain.days + ' 天 ' + remain.hours + ' 時 ' + remain.minutes + ' 分 ' + remain.seconds + ' 秒');
		$('title').html('朵粒濕已經離開' + remain.days + ' 天 ' + remain.hours + ' 時 ' + remain.minutes + ' 分 ' + remain.seconds + ' 秒');
	}

	$(function()
	{
		countdown();
		setInterval(countdown, 1000);
		$('body').sakura();

		$(".typed").typed({
            strings: {!!json_encode($words)!!},
            typeSpeed: 0,
            loop: true
        });
	});
</script>
</body>
</html>

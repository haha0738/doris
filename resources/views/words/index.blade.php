@extends('layout')
@section('content')
<table class="table">
	<thead>
		<tr>
			<th>叫啥</th>
			<th>想說啥</th>
		</tr>
	</thead>
	<tbody>
	@foreach($words as $word)
		<tr>
			<td>{{$word->name}}</td>
			<td>{{$word->content}}</td>
		</tr>
	@endforeach
	</tbody>
</table>
@stop